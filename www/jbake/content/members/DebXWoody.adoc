= DebXWoody 
DebXWoody
2018-06-14
:jbake-type: member
:jbake-status: published
:jbake-tags: member, DebXWoody 
:jbake-updated: 2018-06-14
:idprefix:
:toc:
:sectanchors:
:sectlinks:
:jbake-membernamegitlab: DebXWoody
:jbake-membernameirc: DebXWoody
:jbake-memberurlmastodon: https://social.tchncs.de/@DebXWoody
:jbake-membergpgid: 0x017FD314
:jbake-membergpgfingerprint: 12F1 4755 7BD6 5E78 5ED8 11BB ED1F 87CE 017F D314
:jbake-memberlinuxdistribution: Debian
:jbake-memberlinuxwindowmanager: i3m
:jbake-memberlinuxspachen: Java, C++
:jbake-memberlinuxseit: 2000
:jbake-memberdevlugseit: 2017
:jbake-memberdevluggroup: admin, dev

[abstract]
Linux User seit 2000. Gründer der devLUG.

== Desktop



