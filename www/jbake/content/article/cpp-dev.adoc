= Softwareentwicklung in C/C++
DebXWoody
2018-07-22
:jbake-type: article
:jbake-status: published
:jbake-tags: c, cpp, dev
:jbake-updated: So 22. Jul 08:30:20 CEST 2018
:jbake-autoren: DebXWoody
:jbake-regal: Softwareentwicklung
:idprefix:
:toc:
[abstract]
Ein kleiner Artikel über Softwareentwicklung in C/C++ unter Linux.

== Einleitung

=== Hello World

.main.cpp
[source,cpp]
----
#include <stdlib.h>
#include <iostream>

int main(int argc, char** argv) {
	std::cout << "Hello /dev/LUG" << std::endl;
	return EXIT_SUCCESS;
}
----

=== Makefile
[source,makefile]
----
main: main.cpp
	g++ -o $@ $<
----

.Makefile 
|===
| Name |Beschreibung

|$@
|Name des Ziels

|$<
|Name der 1. Abhängigkeit
|===


