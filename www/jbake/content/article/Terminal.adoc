= Terminal Anwendungen 
DebXWoody
2018-04-07
:jbake-type: article
:jbake-status: published
:jbake-tags: artikel 
:jbake-regal: Anwendungen
:jbake-autoren: DebXWoody 
:jbake-updated: Mi 20. Jun 20:08:42 CEST 2018
:idprefix:
:toc:
[abstract]
Wofür baucht man X? Es gibt viele sehr gute Terminal-Anwendungen. Wir stellen
hier mal ein paar davon vor.

image:img/article/Terminal-Apps.png[]

== Vim - Editor
Vim steht für Vi IMproved und ist ein verbesserter vi-Editor. Der Editor hat
eigentlich fast alles was man so braucht.

image:img/article/vim.png[]

== Neomutt - E-Mail
Ein sehr flexibles E-Mail-Programm. Wer Konsolen-Anwendungen bevorzugt, der wird
neomutt lieben. ;-)

image:img/article/neomutt.png[]

== abook - Adressbuch
Ein ncurses Adressbuch. Abook hat eine query-Funktion um abook in neomutt zu
integrieren.

image:img/article/abook.png[]

http://abook.sourceforge.net/

	aptitude install abook

== Taskwarrior - Aufgaben
Aufgaben und ToDo-Liste.

https://taskwarrior.org

	aptitude install timewarrior

== Timewarrior - Zeiterfassung
Zeiterfassung

image:img/article/timew800.png[]

	aptitude install timewarrior

Dokumentation: https://taskwarrior.org/docs/timewarrior/

== irssi - IRC-Client

