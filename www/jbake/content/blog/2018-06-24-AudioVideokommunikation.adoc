= Audio-/Videokommunikation 
DebXWoody
2018-06-24
:jbake-type: post
:jbake-status: published
:jbake-tags: audio
:idprefix:
== Audio-/Videokommunikation
Dieses Wochenende haben wir ein paar Tests mit Tools für Audiokommunikation gemacht.

Wir haben uns SIP-Accounts auf http://linphone.org/ und  http://opensips.org/ angelegt.
Als Client-Software linphone, empathy mit telepathy-rakia. Auf dem Handy haben wir linphone und Ring installiert.

Für Mumble brauchen wir einen Server, deswegen ist es für uns aktuell noch keine Lösung.

Leider waren unserer Tests noch nicht so positiv. Eine Verbindung von Ring auf Handy zu Handy via Ring-Protokoll war erfolgreich.

Mal sehen wie die Sache weitergeht,..

