= vLUG-Session
DebXWoody
2018-06-25
:jbake-type: devlug
:jbake-status: published
:jbake-tags: devlug, session
:jbake-updated: 2018-06-25
:idprefix:
:sectanchors:
:sectlinks:
:toc:
[abstract]
Hier findest du Informationen zur vLUG-Session. 

== Was sind vLUG-Sessions?
vLUG-Sessions sind Audio- Video-Konferenzen mit oder ohne Screensharing der devLUG.

== Wunschliste

* Einführung in ansible
* Grundlagen Taskwarrior und Timewarrior

== Archiv

=== E-Mail mit neomutt und co
Am Montag, 25.06.2018 um 19:00 Uhr hatten wir unsere 1. vLUG-Session. Das Thema war neomutt und co.
Mit 4 Personen hatten wir eine Videokonferenz gemacht und einen Bildschirm freigegeben.

* Übersicht getmail, procmail, notmuch
* Grundlagen neomutt 


