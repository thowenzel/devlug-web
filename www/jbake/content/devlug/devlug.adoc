= Devlug 
DebXWoody
2018-05-27
:jbake-type: devlug
:jbake-status: published
:jbake-tags: 
:jbake-updated: 2018-06-10
:idprefix:
:toc:
[abstract]
Hier findest du alle Informationen zur devLUG.

== Die devLUG
Allgemeine Informationen zur devLUG findest du auf der Seite
link:https://www.devlug.de/about.html[Über uns]. Die
link:https://www.devlug.de/devlug/history.html[Geschichte der devLUG] haben wir
in einer Tabelle zusammengetragen. 

== Kommunikation 
Die Kommunikation innerhalb der devLUG läuft primäre über die
link:https://www.devlug.de/devlug/mailinglisten.html[Mailinglisten]. Wir haben
eine Mailingliste für die Zusammenarbeit und eine für Ankündigungen und wichtige
Informationen.

Neben der Mailingliste nutzen wir noch den Internet Relay Chat (IRC) auf
freenode.net. Hier findet auch unser link:https://www.devlug.de/devlug/stammtisch.html[Stammtisch] statt.

Auf Mastodon kannst du unserem link:https://social.tchncs.de/@devlug[Account @devlug] folgen.

== Projekte
Wir arbeiten an link:https://www.devlug.de/projects.html[Projekten] welche auf GitLab
verwaltet werden. Die meisten Aufgaben werden dann innerhalb von
link:https://www.devlug.de/devlug/vlug-sprints.html[vLUG-Sprints] be- bzw.
erarbeitet.

So haben wir beispielsweise in unserm Projekt
link:https://www.devlug.de/project/infrastruktur.html[Infrastruktur] unsere
Homepage, welche dann im Projekt https://gitlab.com/devlug/devlug-web auf GitLab
verwaltet wird. 

== Dokumentation
Auf unserer Homepage haben wir mehrere
link:https://www.devlug.de/devlug/dokumente.html[Dokumente] an denen wir
zusammen arbeiten können. link:https://www.devlug.de/devlug/members.html[Mitgliederseiten], 
link:https://www.devlug.de/archive.html[Blog-Einträge], link:https://www.devlug.de/tags/refcard.html[Referenzkarten]
und link:https://www.devlug.de/articles.html[Artikel] - hier findet sicher jeder ein Platz etwas zu dokumentieren.   

== Lernen und verstehen
Um miteinander und voneinander zu lernen, haben wir die
link:https://www.devlug.de/devlug/vlug-session.html[vLUG-Session] geschaffen.
Audio- / Video- / Screensharing-Konferenzen um etwas vorzutragen, zu diskutieren
oder zu erarbeiten.

== Mitmachen
Wenn du mitmachen willst, dann solltest du dir unser
link:https://www.devlug.de/devlug/contributionguide.html[Contribution Guide]
ansehen.

