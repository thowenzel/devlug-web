#!/bin/bash

NAME=`date +%FT%H%M`
echo Erstelle Tag ${NAME}

git tag -s -m 'Tag nach Deployment' ${NAME}
git push --tags

